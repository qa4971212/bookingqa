import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$x;

public class SearchPage {

    public final static By CITY_HEADER = By.xpath("//h1");
    public final static By OBJECT_RATE = By.cssSelector("#filter_group_class_\\:Rsq\\:");
    public final static String HOTEL_CHECKBOX_RATE = "div[data-filters-item='class:class=%s']";
    public final static By CHECKBOX = By.cssSelector("span.bbdb949247");

    public static final By HOTEL_RATING = By.cssSelector("div[data-testid='rating-stars']");

    public static final By STAR = By.cssSelector("span");

    public static final By SHOW_ON_MAP_BUTTON = By.xpath("//div[@data-testid='map-trigger' and @role='button']");

    public static final String HOTEL = "//div[@data-testid='property-card'][2]";

    public static final String NAME = "//*[@data-testid='title']";

    public static final String STARS = "//*[@data-testid='rating-stars']//span";

    public static final String AVERAGE_RATING = "//div[contains(@aria-label, 'Оценка ') and contains(@aria-label, ',')]";

    public static final String NUMBER_OF_REVIEWS = "//div[contains(text(), 'отзыв')]";

    public static final String PRICE = "//span[@data-testid='price-and-discounted-price']";

    public SearchPage checkCityHeader(String city) {
        $(CITY_HEADER).shouldHave(Condition.text(city));
        return this;
    }

    public SearchPage setRateCheckbox(String rate) {
        $(String.format(HOTEL_CHECKBOX_RATE, rate)).find(CHECKBOX).click();
        return this;
    }

    public SearchPage checkHotelRating(String rate) {
        ElementsCollection hotels = $$(HOTEL_RATING);
        for (SelenideElement hotel: hotels) {
            hotel.findAll(STAR).shouldHave(CollectionCondition.size(Integer.parseInt(rate)));
        }
        return this;
    }

    public SearchPage showOnMap() {
        $(SHOW_ON_MAP_BUTTON).shouldBe(Condition.interactable, Duration.ofSeconds(10)).click();
        return this;
    }

    public Hotel saveHotelData() {
        SelenideElement hotelElement = $x(HOTEL).shouldBe(Condition.visible, Duration.ofSeconds(10));

        String name = $x(HOTEL + NAME).getText();
        ElementsCollection starsElements = $$x(HOTEL + STARS);
        String stars = Integer.toString(starsElements.size());
        String averageRating = $x(HOTEL + AVERAGE_RATING).getText();
        String numberOfReviews = $x(HOTEL + NUMBER_OF_REVIEWS).getText();
        String price = $x(HOTEL + PRICE).getText();

        Hotel hotel = new Hotel(name, stars, averageRating, numberOfReviews, price);
        return  hotel;
    }

    public SearchPage clickOnHotel() {
        $x(HOTEL + NAME).click();
        switchTo().window(1);
        return this;
    }

}
