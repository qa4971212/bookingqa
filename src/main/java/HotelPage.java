import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class HotelPage {
    public static final By NAME = By.xpath("//div[@id='hp_hotel_name']//h2");

    public static final By STARS = By.xpath("//span[@data-testid='rating-stars']");
    public static final By AVERAGE_RATING = By.xpath("//*[@data-testid='review-score-right-component']//*[contains(@aria-label, 'Оценка') and contains(text(), ',')]");
    public static final By NUMBER_OF_REVIEWS = By.xpath("//*[@data-testid='review-score-right-component']//*[contains(text(), 'отзыв')]");
    public static final By PRICE = By.xpath("//table[@id='hprt-table']//*[contains(@class, 'bui-price-display')]//*[@class='prco-valign-middle-helper']");

    public Hotel getHotel() {
        String name = $(NAME).shouldBe(Condition.exist, Duration.ofSeconds(10)).text();

        ElementsCollection starsElements = $$(STARS);
        String stars = Integer.toString(starsElements.size());
        String averageRating = $(AVERAGE_RATING).getText();
        String numberOfReviews = $(NUMBER_OF_REVIEWS).getText();
        String price = $(PRICE).getText();
        return new Hotel(name, stars, averageRating, numberOfReviews, price);
    }
}
