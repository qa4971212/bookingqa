import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.devtools.v109.page.model.PrerenderFinalStatus;

import static com.codeborne.selenide.Selenide.$;

public class HomePage {


    public static final  By ACCEPT_COOKIE_BUTTON = By.xpath("//button[@id='onetrust-accept-btn-handler']");
    public static final  By SEARCH_FIELD = By.xpath("//input[@id=':Ra9:']");
    public static final  By SUBMIT_BUTTON = By.xpath("//button[@type='submit']");
    public static final  By FIRST_IN_DROPDOWN = By.cssSelector("div.a40619bfbe");

    public static final By DATA_START = By.xpath("//button[@data-testid='date-display-field-start']");
    public static final By DATA_END = By.xpath("//button[@data-testid='date-display-field-end']");

    public static final By DATA_TABLES = By.xpath("//div[@class='fa3f76ae6b']");
    public static final By DATA_TABLES_COLLECTION = By.xpath("div[@class='f261b68fe6']");

    public static final By DATA1 = By.xpath("//div[@class='fa3f76ae6b']//div[@class='f261b68fe6']//tbody//td[@role='gridcell']/span[@data-date='2023-04-20']");
    public static final By DATA2 = By.xpath("//div[@class='fa3f76ae6b']//div[@class='f261b68fe6']//tbody//td[@role='gridcell']/span[@data-date='2023-04-21']");

    public static final By CLOSE_GENIUS = By.xpath("//button[@aria-label='Скрыть меню входа в аккаунт.']");


    public HomePage openHomePage() {
        Selenide.open("https://www.booking.com/");
        return this;
    }

    public HomePage acceptCookie() {
        $(ACCEPT_COOKIE_BUTTON).shouldBe(Condition.visible).click();
        return this;
    }
    public HomePage fillCity(String city) {
        $(SEARCH_FIELD).sendKeys(city);
        $(FIRST_IN_DROPDOWN).shouldHave(Condition.text(city));
        return this;
    }

    public HomePage search() {
        $(SUBMIT_BUTTON).click();
        return this;
    }


    //Modify this method to pass data to method
    // chooseRandomData(String data)
    public HomePage chooseRandomData() {
        $(DATA_START).click();
        $(DATA1).click();
        $(DATA2).click();
        return this;
    }

    public HomePage closeGeniusLogin() {
        $(CLOSE_GENIUS).click();
        return this;
    }
}