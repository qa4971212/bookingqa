import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.HoverOptions;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.*;

public class MapPage {
    public static final String HOTEL = "//a[@data-testid='property-list-map-card'][2]";

    public static final String NAME = "//span[@data-testid='header-title']";

    public static final String STARS = "//span[@data-testid='rating-stars']";
    public static final String AVERAGE_RATING = "//div[contains(@aria-label, 'Оценка ') and contains(@aria-label, ',')]";
    public static final String NUMBER_OF_REVIEWS = "//div[contains(text(), 'отзыв')]";
    public static final String PRICE = "//div[@data-testid='price-and-discounted-price']//span[2]";

    public static final String ICON = "//div[contains(@class, 'bounce')]";

    public Hotel getHotel() {
        SelenideElement hotelElement = $x(HOTEL).shouldBe(Condition.visible, Duration.ofSeconds(10));

        String name = $x(HOTEL + NAME).getText();
        ElementsCollection starsElements = $$x(HOTEL + STARS);
        String stars = Integer.toString(starsElements.size());
        String averageRating = $x(HOTEL + AVERAGE_RATING).getText();
        String numberOfReviews = $x(HOTEL + NUMBER_OF_REVIEWS).getText();
        String price = $x(HOTEL + PRICE).getText();

        Hotel hotel = new Hotel(name, stars, averageRating, numberOfReviews, price);
        return  hotel;
    }

    public MapPage clickOnHotelIcon() {
        SelenideElement hotelElement = $x(HOTEL).shouldBe(Condition.visible, Duration.ofSeconds(10));
        hotelElement.hover();

        SelenideElement hotelMapIcon = $x(ICON);
        hotelMapIcon.click();
        switchTo().window(1);
        return this;
    }
}
