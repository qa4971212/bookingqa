public class Hotel {

    private String name;
    private String stars;
    private String averageRating;
    private String numberOfReviews;
    private String price;

    public Hotel(String name, String stars, String averageRating, String numberOfReviews, String price) {
        this.name = name;
        this.stars = stars;
        this.averageRating = averageRating;
        this.numberOfReviews = numberOfReviews;
        this.price = price;
    }

    public Hotel() {
    }

    @Override
    public String toString() {
        return "Hotel{" +
        "name='" + name + '\'' +
        ", stars='" + stars + '\'' +
        ", averageRating='" + averageRating + '\'' +
        ", numberOfReviews='" + numberOfReviews + '\'' +
        ", price='" + price + '\'' +
        '}';
    }
}
