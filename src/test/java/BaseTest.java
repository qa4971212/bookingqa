import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;

public class BaseTest {

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
        Configuration.driverManagerEnabled = false;
        Configuration.headless = false;
        Configuration.browserSize = "1920x1080";
    }

    @After
    public void turnDown() {
        Selenide.closeWebDriver();
    }
}
