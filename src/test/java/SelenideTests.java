import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SelenideTests extends BaseTest {

    HomePage homePage = new HomePage();
    SearchPage searchPage = new SearchPage();
    MapPage mapPage = new MapPage();
    HotelPage hotelPage = new HotelPage();

    private String city = "Лиссабон";

    @Test
    public void firstTestScenario() {
//        1.1 зайти на сайт https://www.booking.com/
//        1.2 ввести в поиске любой город(заграничный)
//        1.3 выбрать случайные даты
//        1.4 нажать на кнопку «Найти»
//        1.5 нажать на кнопку «Показать на карте»
//        1.6 навести курсор на первый отель(карточка слева)
//        1.7 сохранить(в переменные) название отеля, количество звезд, среднюю оценку, количество отзывов, стоимость
//        1.8 нажать на прыгающий маркер на карте
//        1.9 на открывшейся странице отеля проверить название отеля, количество звезд, среднюю оценку, количество отзывов и стоимость

        homePage.openHomePage().acceptCookie().fillCity(city).chooseRandomData().search();
        searchPage.showOnMap();
        Hotel hotelFromMapPage = mapPage.getHotel();
        mapPage.clickOnHotelIcon();

        Hotel hotelFromHotelPage = hotelPage.getHotel();
        System.out.println(hotelFromMapPage);
        System.out.println(hotelFromHotelPage);

        Assert.assertEquals(hotelFromMapPage, hotelFromHotelPage);
    }

    @Test
    public void secondTestScenario() {
//        1.1 зайти на сайт https://www.booking.com/
//        1.2 ввести в поиске любой город(заграничный)
//        1.3 выбрать случайные даты
//        1.4 нажать на кнопку «Найти»
//        1.5 Выбрать любой отель на странице с поиском
//        1.6 сохранить(в переменные) название отеля, количество звезд, среднюю оценку, количество отзывов, стоимость
//        1.7 Нажать на отель, проверить, что данные совпадают
        homePage.openHomePage().acceptCookie().fillCity(city).chooseRandomData().search();

        Hotel hotelFromSearchPage = searchPage.saveHotelData();

        searchPage.clickOnHotel();

        Hotel hotelFromHotelPage = hotelPage.getHotel();

        System.out.println(hotelFromSearchPage);
        System.out.println(hotelFromHotelPage);

        Assert.assertEquals(hotelFromSearchPage, hotelFromHotelPage);
    }
}
